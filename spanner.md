# Spanner

- https://cloud.google.com/spanner
- [Spanner: Google’s Globally-Distributed Database](https://static.googleusercontent.com/media/research.google.com/en//archive/spanner-osdi2012.pdf)
- [Spanner: Becoming a SQL System](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/46103.pdf)
- [Доклад и слайды](https://www.usenix.org/conference/osdi12/technical-sessions/presentation/corbett)
- [Spanner, TrueTime and the CAP Theorem](https://research.google/pubs/pub45855/)
- [Cloud Spanner: TrueTime and external consistency](https://cloud.google.com/spanner/docs/true-time-external-consistency)
- https://spanner.fyi/

# CockroachDB

- https://github.com/cockroachdb/cockroach
- [Architecture Overview](https://www.cockroachlabs.com/docs/v20.2/architecture/overview.html)
- [Design Doc](https://github.com/cockroachdb/cockroach/blob/master/docs/design.md)

## Транзакции

- https://www.cockroachlabs.com/tags/transactions/

# YandexDB

- https://cloud.yandex.ru/services/ydb
- [Yandex Database: база данных newSQL](https://www.youtube.com/watch?v=FwLvAuOSIOU)

## Транзакции

- [Calvin: Fast Distributed Transactions for Partitioned Database Systems](http://cs.yale.edu/homes/thomson/publications/calvin-sigmod12.pdf)
- [Распределенные транзакции в YDB](https://www.highload.ru/moscow/2019/abstracts/5324)

