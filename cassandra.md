# Cassandra

- [Dynamo: Amazon’s Highly Available Key-value Store](http://www.cs.cornell.edu/courses/cs5414/2017fa/papers/dynamo.pdf)

## LWT

- [Lightweight transactions in Cassandra 2.0](https://www.datastax.com/blog/lightweight-transactions-cassandra-20)
- [Paxos Made Simple](https://lamport.azurewebsites.net/pubs/paxos-simple.pdf)
- [ScyllaDB / Lightweight Transactions](https://docs.scylladb.com/using-scylla/lwt/), [Getting the Most out of Lightweight Transactions in Scylla](https://www.scylladb.com/2020/07/15/getting-the-most-out-of-lightweight-transactions-in-scylla/)
