# Kafka

* [The Log: What every software engineer should know about real-time data's unifying abstraction](https://engineering.linkedin.com/distributed-systems/log-what-every-software-engineer-should-know-about-real-time-datas-unifying)
* [Power of the Log:LSM & Append Only Data Structures](https://www.infoq.com/presentations/lsm-append-data-structures/)

## Lambda 

* http://lambda-architecture.net/

## API

* https://kafka.apache.org/20/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html
* https://kafka.apache.org/20/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html

## Design
* [Documentation](http://kafka.apache.org/documentation/#design)
* [Kafka Internals](https://www.oreilly.com/library/view/kafka-the-definitive/9781491936153/ch05.html)
* [Kafka Improvement Proposals](https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Improvement+Proposals)

## Replication Protocol
* [Distributed Consensus Reloaded: Apache ZooKeeper and Replication in Apache Kafka](https://www.confluent.io/blog/distributed-consensus-reloaded-apache-zookeeper-and-replication-in-kafka/)
* [ZooKeeper Overview](https://zookeeper.apache.org/doc/current/zookeeperOver.html)
* [Hardening Kafka Replication](https://www.confluent.io/kafka-summit-sf18/hardening-kafka-replication/), [TLA+](https://github.com/hachikuji/kafka-specification)
* [Apache Kafka Needs No Keeper: Removing the Apache ZooKeeper Dependency](https://www.confluent.io/blog/removing-zookeeper-dependency-in-kafka/)

## Transactions & Exactly-once semantics
* [Transactions in Apache Kafka](https://www.confluent.io/blog/transactions-apache-kafka/)
* [Introducing Exactly Once Semantics in Apache Kafka](https://www.confluent.io/kafka-summit-nyc17/introducing-exactly-once-semantics-in-apache-kafka/)
* [Exactly Once Delivery and Transactional Messaging in Kafka Design Doc](https://docs.google.com/document/d/11Jqy_GjUGtdXJK94XGsEIK7CP1SnQGdp2eF0wSw9ra8/edit)
* [Разбор семантики «exactly once» Apache Kafka](https://www.youtube.com/watch?v=PgkRhlUwYyE), [слайды](https://assets.ctfassets.net/oxjq45e8ilak/1y637HHnSQQMewS0m4usYS/95ffde03d09c3f49dcc7fe85fc976553/Gamov_Kafka_EOS.pdf)

## Rebalancing Protocol
* [The Unofficial Kafka Rebalance How-To](https://tomlee.co/2019/03/the-unofficial-kafka-rebalance-how-to/#rebalances-as-double-barriers)

## Streams

* [Tutorial](https://docs.confluent.io/current/streams/quickstart.html), [WordCountLambdaExample.java](https://github.com/diegoicosta/kafka-confluent-examples/blob/master/kafka-streams/src/main/java/io/confluent/examples/streams/WordCountLambdaExample.java)
* [Kafka Streams Concepts](https://docs.confluent.io/current/streams/concepts.html)
* [Enabling Exactly-Once in Kafka Streams](https://www.confluent.io/blog/enabling-exactly-once-kafka-streams/)
* https://www.confluent.io/blog/kafka-streams-tables-part-1-event-streaming/
* https://www.confluent.io/blog/kafka-streams-tables-part-4-elasticity-fault-tolerance-advanced-concepts/
* [Streams and Tables: Two Sides of the Same Coin](https://www.confluent.io/wp-content/uploads/streams-tables-two-sides-same-coin.pdf), [slides](https://db.cs.pitt.edu/birte2018/slides/birte_2018_published.pdf)